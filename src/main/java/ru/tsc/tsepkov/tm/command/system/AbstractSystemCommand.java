package ru.tsc.tsepkov.tm.command.system;

import ru.tsc.tsepkov.tm.api.ICommandService;
import ru.tsc.tsepkov.tm.command.AbstractCommand;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

    @Override
    public String getArgument() {
        return null;
    }

}
