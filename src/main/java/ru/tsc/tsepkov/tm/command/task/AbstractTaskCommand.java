package ru.tsc.tsepkov.tm.command.task;

import ru.tsc.tsepkov.tm.api.IProjectTaskService;
import ru.tsc.tsepkov.tm.api.ITaskService;
import ru.tsc.tsepkov.tm.command.AbstractCommand;
import ru.tsc.tsepkov.tm.enumerated.Status;
import ru.tsc.tsepkov.tm.model.Task;

public abstract class AbstractTaskCommand extends AbstractCommand {

    protected ITaskService getTaskService() {
        return serviceLocator.getTaskService();
    }

    protected IProjectTaskService getProjectTaskService() {
        return serviceLocator.getProjectTaskService();
    }

    @Override
    public String getArgument() {
        return null;
    }

    public void showTask(final Task task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
    }

}
