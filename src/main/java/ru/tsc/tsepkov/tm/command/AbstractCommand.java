package ru.tsc.tsepkov.tm.command;

import ru.tsc.tsepkov.tm.api.ICommand;
import ru.tsc.tsepkov.tm.api.IServiceLocator;

public abstract class AbstractCommand implements ICommand {

    protected IServiceLocator serviceLocator;

    public abstract String getArgument();

    public abstract String getName();

    public abstract String getDescription();

    public abstract void execute();

    public IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    public void setServiceLocator(final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public String toString() {
        final String name = getName();
        final String argument = getArgument();
        final String description = getDescription();
        String displayName = "";
        if (name != null && !name.isEmpty()) displayName += name;
        if (argument != null && !argument.isEmpty()) displayName += ", " + argument;
        if (description != null && !description.isEmpty()) displayName += ": " + description;
        return displayName;
    }

}
