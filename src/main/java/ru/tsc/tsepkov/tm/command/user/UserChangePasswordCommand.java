package ru.tsc.tsepkov.tm.command.user;

import ru.tsc.tsepkov.tm.util.FormatUtil;
import ru.tsc.tsepkov.tm.util.TerminalUtil;

public class UserChangePasswordCommand extends AbstractUserCommand {

    public static final String NAME = "change-user-password";

    public static final String DESCRIPTION = "Change password of current user.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[USER CHANGE PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        final String password = TerminalUtil.nextLine();
        serviceLocator.getUserService().setPassword(userId, password);
    }

}
