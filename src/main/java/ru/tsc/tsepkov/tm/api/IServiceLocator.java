package ru.tsc.tsepkov.tm.api;

public interface IServiceLocator {

    ICommandService getCommandService();

    IProjectService getProjectService();

    ITaskService getTaskService();

    IProjectTaskService getProjectTaskService();

    ILoggerService getLoggerService();

    IUserService getUserService();

    IAuthService getAuthService();

}
