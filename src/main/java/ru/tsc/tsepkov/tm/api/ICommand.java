package ru.tsc.tsepkov.tm.api;

public interface ICommand {

    String getArgument();

    String getName();

    String getDescription();

    void execute();

}
