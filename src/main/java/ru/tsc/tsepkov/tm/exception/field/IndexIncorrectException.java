package ru.tsc.tsepkov.tm.exception.field;

public final class IndexIncorrectException extends  AbstractFieldException {

    public IndexIncorrectException() {
        super("Error! Index is incorrect...");
    }

}
