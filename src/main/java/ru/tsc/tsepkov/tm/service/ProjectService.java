package ru.tsc.tsepkov.tm.service;

import ru.tsc.tsepkov.tm.api.IProjectRepository;
import ru.tsc.tsepkov.tm.api.IProjectService;
import ru.tsc.tsepkov.tm.enumerated.Sort;
import ru.tsc.tsepkov.tm.enumerated.Status;
import ru.tsc.tsepkov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.tsepkov.tm.exception.field.*;
import ru.tsc.tsepkov.tm.model.Project;
import java.util.Comparator;
import java.util.List;

public final class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public Project create(final String name, final String description) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null) throw new DescriptionNullException();
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        add(project);
        return project;
    }

    @Override
    public void add(final Project project) {
        if (project == null) throw new ProjectNullException();
        projectRepository.add(project);
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public List<Project> findAll(final Comparator comparator) {
        if (comparator == null) return findAll();
        return projectRepository.findAll(comparator);
    }

    @Override
    public List<Project> findAll(final Sort sort) {
        if (sort == null) return findAll();
        return findAll(sort.getComparator());
    }

    @Override
    public Project findOneById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return projectRepository.findOneById(id);
    }

    @Override
    public boolean existsById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return projectRepository.existsById(id);
    }

    @Override
    public Project findOneByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= projectRepository.getSize()) throw new IndexIncorrectException();
        return projectRepository.findOneByIndex(index);
    }

    @Override
    public Project updateById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= projectRepository.getSize()) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Project project = findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public void remove(final Project project) {
        if (project == null) throw new ProjectNullException();
        projectRepository.remove(project);
    }

    @Override
    public Project removeById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return projectRepository.removeById(id);
    }

    @Override
    public Project removeByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= projectRepository.getSize()) throw new IndexIncorrectException();
        return projectRepository.removeByIndex(index);
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

    @Override
    public int getSize() {
        return projectRepository.getSize();
    }

    @Override
    public Project changeProjectStatusById(final String id, final Status status) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeProjectStatusByIndex(final Integer index, final Status status) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= projectRepository.getSize()) throw new IndexIncorrectException();
        final Project project = findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

}
